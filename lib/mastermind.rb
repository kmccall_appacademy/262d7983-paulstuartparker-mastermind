class Code
  attr_accessor :pegs
  def initialize(pegs)
    if pegs.nil?
      @pegs = PEGS
    else
      @pegs = pegs
    end
  end
  PEGS = {
          red: 'r',
          green: 'g',
          blue: 'b',
          yellow: 'y',
          orange: 'o',
          purple: 'p'
        }

  def self.random
    Code.new([PEGS.values.shuffle[0..3].join("")])
  end

  def [](key)
    @pegs[key]
  end

  def ==(code)
    if code.is_a?(Code)
      return self.pegs == code.pegs
    end
    false
  end

  def self.parse(string)
    code = string.downcase.chars
    if code.all? { |x| PEGS.values.include?(x) }
      return Code.new(code)
    else
      raise "Invalid String"
    end
  end

  def exact_matches(other_code)
    match_count = 0
    4.times do |i|
      if other_code[i] == self[i]
        match_count += 1
      end
    end
    match_count
  end

  def near_matches(other_code)
    hash1 = {}
    hash2 = {}
    4.times do |i|
      unless self[i] == other_code[i]
        if hash1.keys.include?(self[i])
          hash1[self[i]] += 1
        else
          hash1[self[i]] = 1
        end

        if hash2.keys.include?(other_code[i])
          hash2[other_code[i]] += 1
        else
          hash2[other_code[i]] = 1
        end
      end
    end
    nearmatches = 0
    (hash1.keys.uniq & hash2.keys.uniq).each do |color|
      nearmatches += [hash1[color], hash2[color]].min
    end
    nearmatches
  end


end

class Game
  attr_accessor :secret_code
  def initialize(code = Code.random)
    @secret_code = code
  end


  def get_guess
    puts "Input Guess"
    guess = $stdin.gets.chomp
    Code.parse(guess)

  end

  def display_matches(guess)
    near = @secret_code.near_matches(guess)
    exact = @secret_code.exact_matches(guess)
    puts("#{near} near matches")
    puts("#{exact} exact matches")
  end


end
